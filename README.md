## AwesomeCalibre

A symbolic icon theme for Calibre, based on Font Awesome. https://fontawesome.com/


![](theme.png)
